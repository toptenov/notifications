# Сервис управления рассылками

## Оглавление:
- [Инструкция по установке приложения на ПК](#инструкция-по-установке-приложения-на-пк)
- [Инструкция по использованию API](#инструкция-по-использованию-api)
    - [Создание Клиента](#создание-клиента)
    - [Получение данных о списке Клиентов](#получение-данных-о-списке-клиентов)
    - [Получение данных о Клиенте по ID](#получение-данных-о-клиенте-по-id)
    - [Редактирование данных о Клиенте по ID](#редактирование-данных-о-клиенте-по-id)
    - [Удаление данных о Клиенте по ID](#удаление-данных-о-клиенте-по-id)
    - [Создание Рассылки](#создание-рассылки)
    - [Получение данных о списке Рассылок](#получение-данных-о-списке-рассылок)
    - [Получение данных о Рассылке по ID](#получение-данных-о-рассылке-по-id)
    - [Редактирование данных о Рассылке по ID](#редактирование-данных-о-рассылке-по-id)
    - [Удаление данных о Рассылке по ID](#удаление-данных-о-рассылке-по-id)

## Инструкция по установке приложения на ПК:
- Склонируйте репозиторий на персональный компьютер
- Если необходимо - создайте виртуальное окружение и активируйте его командой:
    - Linux и MacOS: `python3 -m venv "venv"` ; `source venv/bin/activate`
    - Windows: `python -m venv "venv` ; `venv\Scripts\activate.bat`
- Запустите команду `pip install -r requirements.txt` из папки с файлом **requirements.txt**
- Запустите локальный сервер командой `python manage.py runserver`
- В общем случае хостом будет выступать `http://localhost:8000/` или `http://127.0.0.1:8000/`

## Инструкция по использованию API:

#### Создание Клиента:
- Эндпоинт: `api/v1/client/`
- Метод: `POST`
- Пример входных данных:
```json
{
    "phone": "75554567922",
    "tag": "kkk",
    "time_zone": "Europe/Moscow"
}
```
- Пример ответа:
```json
{
    "id": 25,
    "phone": "75554567922",
    "cellular_operator_code": "555",
    "tag": "kkk",
    "time_zone": "Europe/Moscow"
}
```
- Дополнительная информация: 
    - Параметр time_zone принимает значения из списка `zoneinfo.available_timezones()` стандартного модуля **zoneinfo**. Если вы введете неверные данные, то в ответе вам прийдёт список допустимых значений для этого параметра
    - Параметр `cellular_operator_code` во входных данных передавать не нужно - он определяется автоматически по номеру телефона
- Пример выполненного запроса в Postman:
![post_client](https://i.imgur.com/v2XkMip.png "post_client")

____

#### Получение данных о списке Клиентов:
- Эндпоинт: `api/v1/client/`
- Медот: `GET`
- Допустимые параметры:
    - `limit` (количество Клиентов на одной странице)
    - `offset` (с какой записи начать отображать Клиентов)
- Пример ответа:
```json
{
    "count": 25,
    "next": "http://127.0.0.1:8000/api/v1/client/?limit=2&offset=3",
    "previous": "http://127.0.0.1:8000/api/v1/client/?limit=2",
    "results": [
        {
            "id": 2,
            "phone": "76664567902",
            "cellular_operator_code": "666",
            "tag": "tag",
            "time_zone": "Europe/Moscow"
        },
        {
            "id": 3,
            "phone": "76664567903",
            "cellular_operator_code": "666",
            "tag": "tag",
            "time_zone": "Europe/Moscow"
        }
    ]
}
```
- Пример выполненного запроса в Postman:
![get_clients](https://i.imgur.com/wpMEbK8.png "get_clients")

____

#### Получение данных о Клиенте по ID:
- Эндпоинт: `api/v1/client/{id}/`
- Метод: `GET`
- Дополнительная информация:
    - ID Клиента вводится в эндпоинт. Например: `api/v1/client/25/`
- Пример ответа:
```json
{
    "id": 25,
    "phone": "75554567924",
    "cellular_operator_code": "555",
    "tag": "kkk",
    "time_zone": "Chile/Continental"
}
```
- Пример выполненного запроса в Postman:
![get_client](https://i.imgur.com/a9ERtKy.png "get_client")

____

#### Редактирование данных о Клиенте по ID:
- Эндпоинт `api/v1/client/{id}/`
- Метод `PUT`
- Дополнительная информация:
    - ID Клиента вводится в эндпоинт. Например: `api/v1/client/7/`
- Пример входных данных:
```json
{
    "phone": "72224567897",
    "tag": "tag1",
    "time_zone": "Asia/Omsk"
}
```
- Пример ответа:
```json
{
    "id": 7,
    "phone": "72224567897",
    "cellular_operator_code": "222",
    "tag": "tag1",
    "time_zone": "Asia/Omsk"
}
```
- Пример выполненного запроса в Postman:
![update_client](https://i.imgur.com/zTDytjE.png "update_client")

____

#### Удаление данных о Клиенте по ID:
- Эндпоинт `api/v1/client/{id}/`
- Метод `DELETE`
- Дополнительная информация:
    - ID Клиента вводится в эндпоинт. Например: `api/v1/client/25/`
- Пример выполненного запроса в Postman:
![delete_client](https://i.imgur.com/EcjEp1J.png "delete_client")

____

#### Создание Рассылки:
- Эндпоинт: `api/v1/mailing/`
- Метод: `POST`
- Пример входных данных:
```json
{
    "date_start": "2022-01-01T00:01:02",
    "text": "Some text",
    "tag": "ttt",
    "cellular_operator_code": "113",
    "date_stop": "2022-01-02T00:01:02"
}
```
- Пример ответа:
```json
{
    "id": 72,
    "cellular_operator_code": "113",
    "date_start": "2022-01-01T00:01:02",
    "text": "Some text",
    "tag": "ttt",
    "date_stop": "2022-01-02T00:01:02"
}
```
- Дополнительная информация: 
    - При создании Рассылки в базе данных создаются сообщения для этой Рассылки, содержащие ID Клиентов, `cellular_operator_code` или `tag`которых совпадает с `cellular_operator_code` и `tag` Рассылки соотвественно
- Пример выполненного запроса в Postman:
![post_mailing](https://i.imgur.com/qGWLOFH.png "post_mailing")

____

#### Получение данных о списке Рассылок:
- Эндпоинт: `api/v1/mailing/`
- Медот: `GET`
- Допустимые параметры:
    - `limit` (количество Рассылок на одной странице)
    - `offset` (с какой записи начать отображать Рассылки)
- Пример ответа:
```json
{
    "count": 63,
    "next": "http://127.0.0.1:8000/api/v1/mailing/?limit=2&offset=12",
    "previous": "http://127.0.0.1:8000/api/v1/mailing/?limit=2&offset=8",
    "results": [
        {
            "id": 18,
            "cellular_operator_code": "113",
            "date_start": "2022-01-01T00:01:02",
            "text": "Some text",
            "tag": "ttt",
            "date_stop": "2022-01-02T00:01:02"
        },
        {
            "id": 19,
            "cellular_operator_code": "113",
            "date_start": "2022-01-01T00:01:02",
            "text": "Some text",
            "tag": "ttt",
            "date_stop": "2022-01-02T00:01:02"
        }
    ]
}
```
- Пример выполненного запроса в Postman:
![get_mailings](https://i.imgur.com/0eqRakP.png "get_mailings")

____

#### Получение данных о Рассылке по ID:
- Эндпоинт: `api/v1/mailing/{id}/`
- Метод: `GET`
- Дополнительная информация:
    - ID Рассылки вводится в эндпоинт. Например: `api/v1/mailing/20/`
- Пример ответа:
```json
{
    "id": 20,
    "cellular_operator_code": "113",
    "date_start": "2022-01-01T00:01:02",
    "text": "Some text",
    "tag": "ttt",
    "date_stop": "2022-01-02T00:01:02"
}
```
- Пример выполненного запроса в Postman:
![get_mailing](https://i.imgur.com/95DrI0P.png "get_mailing")

____

#### Редактирование данных о Рассылке по ID:
- Эндпоинт `api/v1/mailing/{id}/`
- Метод `PUT`
- Дополнительная информация:
    - ID Клиента вводится в эндпоинт. Например: `api/v1/mailing/57/`
    - При редактировании данных о Рассылке в базе данных удаляются Сообщения, соответствующие данной Рассылке и создаются Сообщения для этой Рассылки, содержащие ID Клиентов, `cellular_operator_code` или `tag` которых совпадает с обновленными `cellular_operator_code` и `tag` Рассылки соотвественно 
- Пример входных данных:
```json
{
    "date_start": "2022-01-01T00:01:02",
    "text": "Some text",
    "tag": "tag",
    "cellular_operator_code": "555",
    "date_stop": "2022-01-02T00:01:02"
}
```
- Пример ответа:
```json
{
    "id": 57,
    "cellular_operator_code": "555",
    "date_start": "2022-01-01T00:01:02",
    "text": "Some text",
    "tag": "tag",
    "date_stop": "2022-01-02T00:01:02"
}
```
- Пример выполненного запроса в Postman:
![update_mailing](https://i.imgur.com/A2gnecP.png "update_mailing")

____

#### Удаление данных о Рассылке по ID:
- Эндпоинт `api/v1/mailing/{id}/`
- Метод `DELETE`
- Дополнительная информация:
    - ID Рассылки вводится в эндпоинт. Например: `api/v1/mailing/20/`
- Пример выполненного запроса в Postman:
![delete_mailing](https://i.imgur.com/lGaNSkc.png "delete_mailing")
