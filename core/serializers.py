import zoneinfo

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from core.models import Client, Mailing, Message


class ClientSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    phone = serializers.RegexField(regex=r'^7\d{10}$',
                                   validators=[UniqueValidator(queryset=Client.objects.all())])
    cellular_operator_code = serializers.RegexField(regex=r'^\d{3}$', read_only=True)
    tag = serializers.CharField(max_length=50, allow_blank=True)
    time_zone = serializers.CharField()

    def validate_time_zone(self, value):
        list_of_available_time_zones = tuple(zoneinfo.available_timezones())
        if value not in list_of_available_time_zones:
            raise serializers.ValidationError(f"Недопустимое значение - ожидается значение из списка: "
                                              f"{list_of_available_time_zones}")
        return value

    def create(self, validated_data):
        validated_data["cellular_operator_code"] = validated_data["phone"][1:4]
        return Client.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.phone = validated_data["phone"]
        instance.cellular_operator_code = validated_data["phone"][1:4]
        instance.tag = validated_data["tag"]
        instance.time_zone = validated_data["time_zone"]
        instance.save()
        return instance


class MailingSerializer(serializers.ModelSerializer):
    cellular_operator_code = serializers.RegexField(regex=r'^\d{3}$')

    def validate(self, attrs):
        if attrs["date_start"] >= attrs["date_stop"]:
            raise serializers.ValidationError("Дата начала рассылки позднее даты конца рассылки")
        return attrs

    class Meta:
        model = Mailing
        fields = ("__all__")


class MessageSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    date_send = serializers.DateTimeField(default=None)
    is_sent = serializers.BooleanField(default=False)
    mailing_id = serializers.PrimaryKeyRelatedField(queryset=Mailing.objects.all(), source='mailing')
    client_id = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all(), source='client')

    def create(self, validated_data):
        return Message.objects.create(**validated_data)
