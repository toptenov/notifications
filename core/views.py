from django.db.models import Q
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response

from core.models import Client, Mailing, Message
from core.serializers import ClientSerializer, MailingSerializer, MessageSerializer


def add_message_entities(mailing_serializer):
    clients = Client.objects.filter(
        Q(tag=mailing_serializer.data["tag"]) |
        Q(cellular_operator_code=mailing_serializer.data["cellular_operator_code"])
    )

    for c in clients:
        message_serializer = MessageSerializer(data={
            "mailing_id": mailing_serializer.data["id"],
            "client_id": c.id
        })
        message_serializer.is_valid(raise_exception=True)
        message_serializer.save()


class ClientsAPIView(ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientSingleAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingAPIView(ListCreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def post(self, request):
        # Add a Mailing-entity:
        mailing_serializer = MailingSerializer(data=request.data)
        mailing_serializer.is_valid(raise_exception=True)
        mailing_serializer.save()

        # Add a Message-entities:
        add_message_entities(mailing_serializer)

        return Response(mailing_serializer.data)


class MailingSingleAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def delete(self, request, **kwargs):
        # Delete a Mailing-entity:
        try:
            Mailing.objects.get(pk=kwargs["pk"]).delete()
        except:
            return Response({"detail": "Нет такой рассылки, ID = " + str(kwargs["pk"])},
                            status=status.HTTP_404_NOT_FOUND)

        # Delete associated Message-entities:
        Message.objects.filter(mailing_id=kwargs["pk"]).delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, **kwargs):
        # Update a Mailing-entity:
        try:
            instance = Mailing.objects.get(pk=kwargs["pk"])
        except:
            return Response({"detail": "Нет такой рассылки, ID = " + str(kwargs["pk"])},
                            status=status.HTTP_404_NOT_FOUND)

        mailing_serializer = MailingSerializer(data=request.data, instance=instance)
        mailing_serializer.is_valid(raise_exception=True)
        mailing_serializer.save()

        # Update associated Message-entities:
        Message.objects.filter(mailing_id=kwargs["pk"]).delete()

        add_message_entities(mailing_serializer)

        return Response(mailing_serializer.data)