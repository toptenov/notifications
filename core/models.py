from django.db import models
from django.utils import timezone


class Client(models.Model):
    phone = models.CharField(max_length=11, unique=True)
    cellular_operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=50, blank=True)
    time_zone = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.phone}"


class Mailing(models.Model):
    date_start = models.DateTimeField()
    text = models.CharField(max_length=4000)
    tag = models.CharField(max_length=50)
    cellular_operator_code = models.CharField(max_length=3)
    date_stop = models.DateTimeField()

    def __str__(self):
        return f"{self.pk}"


class Message(models.Model):
    date_send = models.DateTimeField(null=True)
    is_sent = models.BooleanField(default=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name="mailing_id")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="client_id")

    class Meta:
        unique_together = (('mailing', 'client'), )

    def __str__(self):
        return f"{self.pk}"
